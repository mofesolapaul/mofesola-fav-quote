<?php

declare(strict_types=1);

function outputWebsiteResponse()
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'http://website');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $output = curl_exec($ch);
    return $output ?? "No response received.\n";
}

while (true) {
    echo outputWebsiteResponse();
    sleep(5);
}
